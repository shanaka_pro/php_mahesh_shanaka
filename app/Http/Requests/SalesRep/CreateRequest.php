<?php

namespace App\Http\Requests\SalesRep;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'email' => 'required|email|max:100|unique:sales_reps,email',
            'telephone' => 'nullable',
            'current_route_id' => 'required',
            'joined_date' => 'required',
        ];
    }
}
