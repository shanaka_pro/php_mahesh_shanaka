<?php

namespace App\Http\Controllers;

use App\Repositories\Repository;
use App\Http\Requests\SalesRep\CreateRequest;
use App\Http\Requests\SalesRep\UpdateRequest;
use App\Models\CurrentRoute;
use App\Models\SalesRep;

class SalesRepController extends Controller
{
  
   protected $model;
   protected $currentRoute;

   public function __construct(SalesRep $salesRep, CurrentRoute $currentRoute)
   {
      // set the model
      $this->model = new Repository($salesRep);
      $this->currentRoute = new Repository($currentRoute);
   }


   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

   public function index()
   {

      $salesReps = $this->model->getModel()->latest()->paginate(5);

      return view('sales-reps.index', compact('salesReps'))
         ->with('i', (request()->input('page', 1) - 1) * 5);
   }


   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {

      $currentRoutes = $this->currentRoute->getModel()->pluck('address', 'id');

      return view('sales-reps.create', compact('currentRoutes'));
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */

   public function store(CreateRequest $request)
   {

      try {
         $this->model->create($request->all());

         return redirect()->route('sales-rep.index')->with('success', 'Record created successfully');
      } catch (\Exception $e) {

         return redirect()->back()->with('error', $e->getMessage());
      }
   }

   /**
    * Display the specified resource.
    *
    * @param  \App\Models\SalesRep  $salesRep
    * @return \Illuminate\Http\Response
    */

   public function show($id)
   {
      return $this->model->show($id);
   }


   /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\SalesRep  $salesRep
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {


      if ($this->model->show($id)) {

         $model = $this->model->show($id);

         $currentRoutes = $this->currentRoute->getModel()->pluck('address', 'id');

         return view('sales-reps.edit', compact('currentRoutes', 'model'));
      }
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Models\SalesRep  $salesRep
    * @return \Illuminate\Http\Response
    */

   public function update(UpdateRequest $request, $id)
   {

      try {
         $this->model->update($request->all(), $id);

         return redirect()->route('sales-rep.index')->with('success', 'Record updated successfully');
      } catch (\Exception $e) {

         return redirect()->back()->with('error', $e->getMessage());
      }
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\SalesRep  $salesRep
    * @return \Illuminate\Http\Response
    */

   public function destroy($id)
   {

      try {
         $this->model->delete($id);

         return redirect()->route('sales-rep.index')->with('success', 'Record Deleted successfully');
      } catch (\Exception $e) {

         return redirect()->back()->with('error', $e->getMessage());
      }
   }
}
