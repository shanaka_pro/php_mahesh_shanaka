<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesRepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('sales_reps', function (Blueprint $table) {
            $table->id();
            $table->string('name',100);
            $table->string('email',50)->unique();
            $table->string('telephone',100)->nullable();
            $table->date('joined_date');
            $table->unsignedBigInteger('current_route_id');
            $table->text('comment')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('current_route_id')->references('id')->on('current_routes')->onDelete('cascade');
           
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_reps');
    }
}
