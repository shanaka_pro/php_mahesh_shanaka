<?php

namespace Database\Factories;

use App\Models\SalesRep;
use Illuminate\Database\Eloquent\Factories\Factory;

class SalesRepFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SalesRep::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'name' => $this->faker->name,
            'email' =>$this->faker->email,
            'telephone' => $this->faker->phoneNumber,
            'joined_date' => $this->faker->date,
            'current_route_id' => $this->faker->numberBetween(1,10),
            'comment' => $this->faker->text,
        ];
    }
}
