<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();

        // \App\Models\User::factory(1)->create();

        // \App\Models\User::truncate();
        $this->call(UsersTableSeeder::class);
         //\App\Models\User::factory(10)->create();
         \App\Models\CurrentRoute::factory(10)->create();
         \App\Models\SalesRep::factory(10)->create();

    }
}
