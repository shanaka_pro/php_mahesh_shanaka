<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\SalesRep;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;

class SalesRepresentativeTest extends TestCase
{

    use WithoutMiddleware;
    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->item =  SalesRep::factory()->create();

        $this->user = User::first();
        $this->actingAs($this->user);
    }


    public function testCreateSalesRepWithValidParams()
    {

        $response =  $this->actingAs($this->user)
            ->post(route('sales-rep.store'), [
                'name' => $this->faker->name,
                'email' => $this->faker->email,
                'telephone' => $this->faker->phoneNumber,
                'joined_date' => $this->faker->date,
                'current_route_id' => $this->faker->numberBetween(1, 10),
                'comment' => $this->faker->text,

            ]);

        $response->assertSessionHasNoErrors();
    }


    public function testCreateSalesRepWithInvalidParams()
    {

        $response =  $this->actingAs($this->user)
            ->post(route('sales-rep.store'), [
                'name' => null,
                'email' => $this->faker->phoneNumber,
                'telephone' =>  $this->faker->date,
                'joined_date' => $this->faker->date,
                'current_route_id' => $this->faker->numberBetween(1, 10),
                'comment' => $this->faker->text,

            ]);

        $response->assertSessionHasErrors();
    }
}
