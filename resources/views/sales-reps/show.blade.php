    <div class="modal fade text-left" id="ModalShow{{$salesRep->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"> {{ $salesRep->name }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                
                <div class="modal-body">

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>{{ __('ID') }}:</strong>
                            {{ $salesRep->id }}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>{{ __('Full Name') }}:</strong>
                            {{ $salesRep->name }}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>{{ __('Email Address') }}:</strong>
                            {{ $salesRep->email }}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>{{ __('Telephone') }}:</strong>
                            {{ $salesRep->telephone }}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>{{ __('Joined Date') }}:</strong>
                            {{ $salesRep->joined_date }}
                        </div>
                    </div>

                     <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>{{ __('Current Route') }}:</strong>
                            {{ $salesRep->currentRoute->address ?? null }}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>{{ __('Comment') }}:</strong>
                            {{ $salesRep->comment }}
                        </div>
                    </div>

        
                </div>
            </div>
        </div>
    </div>
