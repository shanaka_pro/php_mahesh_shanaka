@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row align-items-center my-4">
                <div class="col">
                    <h2 class="h3 mb-0 page-title">{{ __('Create Sales Representative') }}</h2>
                </div>
                <div class="col-auto">

                    <a href="{{route('sales-rep.index')}}" class="btn btn-primary" style="color:white">
                        <span style="color:white"></span> {{ __('Back to List') }}
                    </a>

                </div>
            </div>
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Dashboard') }}</a></li>
                                <li class="breadcrumb-item"><a href="{{route('sales-rep.index')}}">{{ __('Sales Team') }}</a></li>
                                <li class="breadcrumb-item active">{{ __('Create Sales Representative') }}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card shadow mb-4">
                <div class="card-body">
                    {!! Form::open(array('route' => 'sales-rep.store','method'=>'POST')) !!}
                    @include('sales-reps.includes.form')

                    {!! Form::close() !!}
                </div>
            </div> <!-- / .card -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection
