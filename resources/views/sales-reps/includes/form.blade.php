<div class="row">

    @isset($model)

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::label('ID:') !!}
            {!! Form::text('id', $model->id, array('disabled' => true,'class' => 'form-control')) !!}
        </div>
    </div>

    @endisset

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::label('full_name:*') !!}
            {!! Form::text('name', null, array('placeholder' => 'Enter full name','class' => 'form-control')) !!}
            @error('name')
            <label class="text-danger">{{ $message }}</label>
            @enderror
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::label('email:*') !!}
            {!! Form::text('email', null, array('placeholder' => 'Enter email','class' => 'form-control')) !!}
            @error('email')
            <label class="text-danger">{{ $message }}</label>
            @enderror
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::label('telephone number:') !!}
            {!! Form::number('telephone', null, array('placeholder' => 'Enter telephone number','class' => 'form-control')) !!}
            @error('telephone')
            <label class="text-danger">{{ $message }}</label>
            @enderror
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">

            {!! Form::label('joined_date:*') !!}
            {!! Form::date('joined_date', null, array('placeholder' => 'Select joined date','class' => 'form-control')) !!}
            @error('joined_date')
            <label class="text-danger">{{ $message }}</label>
            @enderror
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">

            {!! Form::label('current_route:') !!}
            {!! Form::select('current_route_id', $currentRoutes, null, ['class' => ['form-control'],'placeholder' => 'Select current route']) !!}
            @error('current_route_id')
            <label class="text-danger">{{ $message }}</label>
            @enderror
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::label('comment:') !!}
            {!! Form::textarea('comment', null, [ 'placeholder' => 'Select comment', 'rows' => '3', 'class' => 'form-control']) !!}
            @error('comment')
            <label class="text-danger">{{ $message }}</label>
            @enderror
        </div>
    </div>

</div>

<div class="col-xs-12 col-sm-12 col-md-12 text-center">
    <button type="submit" class="btn btn-success"> {!! isset($model) ? __('Update') :__('Save') !!} </button>
</div>
