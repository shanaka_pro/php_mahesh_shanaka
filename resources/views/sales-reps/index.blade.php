@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row align-items-center my-4">
                <div class="col">
                    <h2 class="h3 mb-0 page-title">{{ __('Sales Team') }}</h2>
                </div>
                <div class="col-auto">

                    <a href="{{ route('sales-rep.create') }}" class="btn btn-success" style="color:white">
                        <span style="color:white"></span> {{ __('Add New') }}
                    </a>

                </div>
            </div>
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Dashboard') }}</a></li>
                                <li class="breadcrumb-item active">{{ __('Sales Team') }}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="card shadow">
                        <div class="card-body">
                            <!-- table -->
                            <table class="table datatables" id="dataTable-1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Telephone Number') }}</th>
                                        <th>{{ __('Current Route') }}</th>
                                        <th width="280px">{{ __('Action') }}</th>
                                    </tr>
                                </thead>
                                @foreach ($salesReps as $key => $salesRep)
                                <tbody>
                                    <tr>
                                        <td>{{ $salesRep->id }}</td>
                                        <td>{{ $salesRep->name }}</td>
                                        <td>{{ $salesRep->email }}</td>
                                        <td>{{ $salesRep->telephone }}</td>
                                        <td>{{ $salesRep->currentRoute->address ?? null }}</td>

                                        <td>
                                            <a class="btn btn-success" href="#" data-toggle="modal" data-target="#ModalShow{{$salesRep->id}}">{{ __('Show') }}</a>
                                            <a class="btn btn-primary" href="{{ route('sales-rep.edit',$salesRep->id) }}">{{ __('Edit') }}</a>
                                            <a class="btn btn-danger" href="#" data-toggle="modal" data-target="#ModalDelete{{$salesRep->id}}">{{ __('Delete')}}</a>
                                        </td>

                                        @include('sales-reps.show')
                                        @include('sales-reps.delete')

                                    </tr>
                                </tbody>
                                @endforeach
                            </table>

                            {!! $salesReps->render('pagination::bootstrap-4') !!}

                            {{-- {!! $salesReps->render() !!}  --}}
                            <!-- end table -->
                        </div>
                    </div>
                </div> <!-- .col-md-12 -->
            </div> <!-- end section row my-4 -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->


@endsection
